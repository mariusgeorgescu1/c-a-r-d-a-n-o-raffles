# Software Requirements Specification
## C-A-R-D-A-N-O-Raffles  <br> (Commit-And-Reveal-Decentralized-Application-NFT-Outlet Raffles)
Version 1.0 approved
Prepared by Marius Georgescu  
IEEE System Requirements Specification Template


Table of Contents
=================
- [Software Requirements Specification](#software-requirements-specification)
  - [C-A-R-D-A-N-O-Raffles   (Commit-And-Reveal-Decentralized-Application-NFT-Outlet Raffles)](#c-a-r-d-a-n-o-raffles---commit-and-reveal-decentralized-application-nft-outlet-raffles)
- [Table of Contents](#table-of-contents)
  - [Revision History](#revision-history)
  - [1. 📘 Introduction](#1--introduction)
    - [1.1 💡 Purpose](#11--purpose)
    - [1.2 👥 Intended Audience and Reading Suggestions](#12--intended-audience-and-reading-suggestions)
    - [1.3 🌐 Product Scope](#13--product-scope)
      - [🎯 Objectives and Benefits](#-objectives-and-benefits)
      - [🎯 Goals and Business Alignment](#-goals-and-business-alignment)
    - [1.4 🔗 References](#14--references)
  - [2. 🔍 Overall Description](#2--overall-description)
    - [2.1 🌄 Product Perspective](#21--product-perspective)
      - [Context and Origin](#context-and-origin)
      - [Relation to Larger Systems](#relation-to-larger-systems)
      - [System Overview and Interfaces](#system-overview-and-interfaces)
        - [User Interface Component](#user-interface-component)
        - [Wallet Component](#wallet-component)
        - [DApp Service Provider](#dapp-service-provider)
        - [Blockchain Network](#blockchain-network)
      - [High Level Architecture:](#high-level-architecture)
        - [Version 1: Centralized Hosting and Backend Transaction Handling](#version-1-centralized-hosting-and-backend-transaction-handling)
        - [Version 2: Centralized Hosting with Partial Decentralized Transaction Handling](#version-2-centralized-hosting-with-partial-decentralized-transaction-handling)
        - [Version 3: Decentralized Hosting and Transaction Handling](#version-3-decentralized-hosting-and-transaction-handling)
    - [2.2 🎯 Product Functions](#22--product-functions)
      - [NonAuthenticated User](#nonauthenticated-user)
      - [Authenticated User](#authenticated-user)
        - [Participant](#participant)
        - [Organizer](#organizer)
      - [Usecase Diagram](#usecase-diagram)
      - [Contract Usecases](#contract-usecases)
        - [Use Case \[AU.2\]: Create Raffle](#use-case-au2-create-raffle)
        - [Use Case \[AU.3\]: Buy tickets to raffle](#use-case-au3-buy-tickets-to-raffle)
        - [Use Case \[P.2\]: Reveal Secrets for Raffle Tickets](#use-case-p2-reveal-secrets-for-raffle-tickets)
        - [Use Case \[OP.1\]: Close succesfully finalized raffle](#use-case-op1-close-succesfully-finalized-raffle)
        - [Use Case \[O.2\]: Cancel raffle](#use-case-o2-cancel-raffle)
        - [Use Case \[O.3\]: Close expired raffle](#use-case-o3-close-expired-raffle)
        - [Use Case \[O.4\]: Close underfunded raffle](#use-case-o4-close-underfunded-raffle)
        - [Use Case \[O.5\]: Close unrevealed raffle](#use-case-o5-close-unrevealed-raffle)
        - [Use Case \[AU.4a\]: Close exposed underfunded raffle](#use-case-au4a-close-exposed-underfunded-raffle)
        - [Use Case \[AU.4b\]: Close exposed unrevealed raffle](#use-case-au4b-close-exposed-unrevealed-raffle)
    - [2.3 🧑‍🤝‍🧑 User Classes and Characteristics](#23--user-classes-and-characteristics)
      - [Casual Users](#casual-users)
      - [Raffle Organizers](#raffle-organizers)
    - [2.4 🏞️ Operating Environment](#24-️-operating-environment)
    - [2.5 🖌️⛔ Design and Implementation Constraints](#25-️-design-and-implementation-constraints)
      - [Language Requirements](#language-requirements)
      - [Security Considerations](#security-considerations)
      - [Design Conventions or Programming Standards](#design-conventions-or-programming-standards)
    - [2.6 📚 User Documentation](#26--user-documentation)
    - [2.7 🤔 Assumptions and Dependencies](#27--assumptions-and-dependencies)
      - [Assumptions](#assumptions)
      - [Dependencies](#dependencies)
  - [3. 🔌 External Interface Requirements](#3--external-interface-requirements)
    - [3.1 💻 User Interfaces](#31--user-interfaces)
      - [General User Interface Characteristics:](#general-user-interface-characteristics)
      - [Key Components of the User Interface:](#key-components-of-the-user-interface)
    - [3.2 🖥️ Software Interfaces](#32-️-software-interfaces)
  - [4. System Features](#4-system-features)
    - [US-1: View active raffles](#us-1-view-active-raffles)
      - [Acceptance Criteria.](#acceptance-criteria)
    - [US-2: Connect wallet](#us-2-connect-wallet)
    - [US-3:  Create a raffle](#us-3--create-a-raffle)
      - [Acceptance Criteria.](#acceptance-criteria-1)
    - [US-3: Buy ticket to raffle](#us-3-buy-ticket-to-raffle)
      - [Acceptance Criteria.](#acceptance-criteria-2)
    - [US-4:  View my raffles](#us-4--view-my-raffles)
      - [Acceptance Criteria.](#acceptance-criteria-3)
    - [US-5:  View my tickets](#us-5--view-my-tickets)
      - [Acceptance Criteria.](#acceptance-criteria-4)
  - [📄 Appendix A: Analysis Models](#-appendix-a-analysis-models)
      - [State Diagram](#state-diagram)
      - [Class Diagram](#class-diagram)
      - [Sequence Digram](#sequence-digram)
  - [📄 Appendix B: Glossary](#-appendix-b-glossary)
      - [Definitions, Acronyms and Abbreviations](#definitions-acronyms-and-abbreviations)
        - [Definitions](#definitions)
        - [Acronyms and Abbreviations](#acronyms-and-abbreviations)




## Revision History
| Name | Date | Reason For Changes | Version |
| ---- | ---- | ------------------ | ------- |
|      |      |                    |         |
|      |      |                    |         |
|      |      |                    |         |

## 1. 📘 Introduction
### 1.1 💡 Purpose 

This document describes the functional and non-functional requirements for a decentralized application ([DApp](#acronyms-and-abbreviations)) focused on digital assets raffles, based on a commit and reveal scheme ([CRS](#acronyms-and-abbreviations)) as source of randomness for electing the winner.  
The application described in this document will be further refered as [C-A-R-D-A-N-O-Raffles](#acronyms-and-abbreviations).  
This document covers only the application structure and behaviour and does not cover the consesnsus mechanism and communication protocol of the underlying blockchain.

### 1.2 👥 Intended Audience and Reading Suggestions

This document serves as a comprehensive guide detailing the functionalities, scenarios, and acceptance criteria of the application. Intended for developers (to understand and implement the specified functionalities), testers (to create test cases and ensure the application meets the stipulated requirements), and other stakeholders. It provides a clear blueprint of how the application should function and behave under various circumstances.

### 1.3 🌐 Product Scope

Designed to capitalize on the digital assets trend, [C-A-R-D-A-N-O-Raffles](#acronyms-and-abbreviations) is tailored for facilitating creation, management of, and participation in online raffles. 
This application allows users to raffle their digital assets, providing a chance for other users (participants) to win these assets in exchange for purchasing a ticket. By leveraging the commit and reveal scheme it decentralizes trust, ensuring transparency and fairness in the winner selection process.  

#### 🎯 Objectives and Benefits

- **Decentralized Trust**: The primary objective of [C-A-R-D-A-N-O-Raffles](#acronyms-and-abbreviations) is to eliminate the reliance on a central authority for trust. The commit and reveal scheme ensures that the process of picking a winner is transparent and verifiable, thus fostering trust among participants.

- **Digital Asset Monetization**: Users with digital assets can create raffles, offering others the chance to win these assets. This provides a unique avenue for asset owners to monetize their holdings.

- **Broadened Participation**: By allowing participants to buy tickets and potentially win digital assets, the platform democratizes access to these assets, which might be expensive or rare otherwise.

- **Enhanced Engagement**: The thrill of participating in a raffle and the potential to win valuable assets drive user engagement, encouraging more users to join the ecosystem. 

#### 🎯 Goals and Business Alignment

[C-A-R-D-A-N-O-Raffles](#acronyms-and-abbreviations) aligns with the broader trend of decentralization and blockchain adoption. As businesses recognize the importance of trustless systems and digital assets, this platform will position itself as a leader in the decentralized gaming and asset distribution segment.

### 1.4 🔗 References
>List any other documents or Web addresses to which this SRS refers. These may include user interface style guides, contracts, standards, system requirements specifications, use case documents, or a vision and scope document. Provide enough information so that the reader could access a copy of each reference, including title, author, version number, date, and source or location.

## 2. 🔍 Overall Description
### 2.1 🌄 Product Perspective

[C-A-R-D-A-N-O-Raffles](#acronyms-and-abbreviations) is designed to fill a niche in the rapidly evolving digital asset landscape. However, its foundation and philosophy are deeply rooted in the broader shift towards decentralization and blockchain technology.

#### Context and Origin

While raffles have been a popular method of asset distribution and engagement for centuries, the digital age and rise of cryptocurrencies and non-fungible tokens ([NFTs](#acronyms-and-abbreviations)) have reshaped the potential of such events. [C-A-R-D-A-N-O-Raffles](#acronyms-and-abbreviations) is not merely a digital transformation of traditional raffles but a novel system that integrates the principles of decentralization.

#### Relation to Larger Systems

-  **Blockchain** : the current DApp, primarily relies on blockchain technology for it's core functionalities. The underlying blockchain provides the backbone of trust and decentralization and the underlying infrastructure that makes digital assets possible. 
- **IPFS**: Blockchain typically stores metadata that includes information about the digital asset, like an image, video, or piece of music. However, storing large files directly on the blockchain is impractical due to size limitations and high costs. [IPFS](#acronyms-and-abbreviations) offers a solution for decentralized storage. Instead of storing the actual data on blockchain, digital assets metadata often includes a URL or hash pointing to a file on [IPFS](#acronyms-and-abbreviations), utilizing its content addressing system to ensure the integrity and permanence of the data. 

#### System Overview and Interfaces

The diagram below highlights the key logical components of the DApp and their interactions. Each component plays a specific role in the functionality of the DApp, particularly within the context of a blockchain network. 

```plantuml
@startuml
!include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Component/ComponentDiagram-RaffleDApp-HighLevel.puml
@enduml
```
The user directly interacts with the UI, which is the entry point of the DApp. The UI facilitates user interaction with the blockchain through the Wallet component, which securely authorizes transactions. The DApp service provider acts as a bridge, handling complex interactions with the blockchain and ensuring smooth operation of the application. The blockchain network itself is the backbone, providing a decentralized and secure platform for executing the raffle's smart contracts.

Here's an elaborated description of each component:

##### User Interface Component

- **Role**: This component is the front-end of the DApp, where users interact with the application. It presents the user interface through which users can participate in raffles or organize their own.
- **Functions**: It manages user inputs, displays information (like raffle details and results), and initiates actions (like entering the raffle).
- **Interaction**: The UI communicates with the Browser Wallet to create transactions that will be sent to the blockchain.

##### Wallet Component
- **Role**: This is a third-party browser wallet that interacts with the DApp. It handles cryptographic functions related to the user's blockchain identity.
- **Functions**: Its primary function is to sign transactions, which is crucial for actions that require user authorization, such as entering a raffle or claiming rewards.
- **Interaction**: Receives transaction data from the UI, signs it with the user's private key, and then communicates it back to the UI or directly to the blockchain network.


##### DApp Service Provider

- **Role**: This backend component acts as the intermediary between the UI and the blockchain network.
- **Functions**:
  - Querying the Blockchain: It retrieves data from the blockchain, such as the status of the raffle, entries, and winners.
  - Constructing Transactions: It prepares transactions based on user actions that are to be executed on the blockchain.
- **Interaction**: It communicates with the blockchain network to fetch data or submit signed transactions.

##### Blockchain Network 
- **Role**: The underlying decentralized infrastructure that executes and records all operations related to the raffle.
- **Functions**: Hosts the smart contracts which govern the raffle logic, ensuring trustless and transparent execution. It processes transactions, updates the state of the raffle, and ensures the integrity and security of the entire process.
- **Interaction**: Interacts with the DApp service provider, receiving queries and transactions, and returning the results or confirmation of actions taken.


#### High Level Architecture:

There are three architectural versions envisaged for the DApp, each representing a different approach, varying by where the DApp static content is hosted, where transactions are constructed and how transactions are submited to the blockchain.


##### Version 1: Centralized Hosting and Backend Transaction Handling
- **Static Content Hosting**: The DApp's static content, like HTML, CSS, and JavaScript files, is hosted on a centralized web server.
- **Transaction Construction and Submission**: Transactions are constructed and submitted through the DApp's backend. The process involves:
  1. Users interacting with the UI, which calls an endpoint in the OffChain component.
  2. The OffChain component queries the Cardano node and constructs the transaction.
  3. The constructed transaction is sent to the user's browser wallet for signing.
  4. Once signed, the transaction is sent back to the OffChain component, which submits it to the Cardano node for final processing on the blockchain.
```plantuml
@startuml
!include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Component/ComponentDiagram-RaffleDApp-version1.puml
@enduml
```
---
##### Version 2: Centralized Hosting with Partial Decentralized Transaction Handling
- **Static Content Hosting**: Similar to Version 1, the static content is hosted on a centralized server.
- **Transaction Construction**: The construction of transactions still happens in the DApp's backend.
- **Transaction Submission**: The signed transactions are submitted through the user's browser wallet or a custom node configured in the wallet. This adds a layer of decentralization to the process. *(Some wallets allow configuring a custom endpoint for submitting transactions, offering more flexibility and control to the user.)*

```plantuml
@startuml
!include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Component/ComponentDiagram-RaffleDApp-version2.puml
@enduml
```
---

##### Version 3: Decentralized Hosting and Transaction Handling
- **Static Content Hosting**: The DApp's static content is hosted on the IPFS network, offering a decentralized hosting solution.
- **Transaction Construction and Submission**:
    1.  The UI queries data from a Blockchain Services Provider.
    2. Transactions are constructed directly in the user's browser and sent to the browser wallet for signing.
    3. The signed transactions are then submitted to the blockchain network through the wallet's backend or a custom node, emphasizing decentralized processing.

```plantuml
@startuml
!include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Component/ComponentDiagram-RaffleDApp-version3.puml
@enduml
```
--- 


Each version offers different levels of decentralization and control:

- **Version 1** focuses on a more traditional web application structure with centralized control over transaction processing and content hosting.
- **Version 2** introduces an element of decentralization in transaction submission while keeping the content hosting and transaction construction centralized.
- **Version 3** fully embraces decentralization by leveraging IPFS for hosting and allowing transactions to be constructed and submitted directly from the user's browser, reducing reliance on a centralized backend.



### 2.2 🎯 Product Functions

Below are summarized the major functions of the DApp, grouped based on the user classes:


#### NonAuthenticated User
- **Characteristics** : Represents users who have not connected a wallet. They have limited access to the DApp features compared to authenticated users.
- **Interactions** 
  - Can view active raffles.
  - Can connect to a wallet, which is a prerequisite for further interactions within the DApp.

#### Authenticated User
- **Characteristics** : Users who have successfully connected to a wallet. They have enhanced access and functionalities compared to non-authenticated users. This role forms the basis for more specialized roles like Participant and Organizer.
- **Interactions** :
  - Can view active raffles.
  - Can disconnect from the wallet.
  - View his own assets.
  - Create raffles.
  - Buy tickets for any of the active raffles.

##### Participant
- **Characteristics**  Authenticated users who actively participate in at least one raffle.
- **Interactions** 
  - Participate in raffles.
  - View raffles joined and manage participation 
  - Redeem prize.

##### Organizer
- **Characteristics** : Authenticated users who actively organize at least one raffle. They have administrative capabilities within the context of raffle management.
- **Interactions** 
  - View raffles they have created.
  - Cancel raffles.
  - Close raffles.


#### Usecase Diagram


```plantuml
@startuml
!include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Usecase/UsecaseDiagram-RaffleDApp.puml
@enduml
```



#### Contract Usecases

##### Use Case [AU.2]: Create Raffle
---
**Actors**: Authenticated User, RaffleValidator    
**Summary Description**:	This transaction allows anyone to create a raffle for some digital assets.  

**Preconditions:**
1. The organizer must own the raffle prize value.


**Postconditions:**
1. Raffle prize value is locked to the raffle validator's address with valid datum. 


**Transaction:**

The transaction must mint a **State Thread Token NFT** wich will be used to track the raffle state. 
 * The raffle parameters of the minting policy  must be equal with the ones in the datum of the output locked at raffle validator's address.
 * The raffle datum must be a valid datum to create a new raffle.
 * The transaction must mint exactly 1 state token (with token name equal to the seed 'TxOutRef').
 * The transaction must have an output with value containing the raffle's prize value + raffle state thread NFT and a valid datum, locked at the raffle validator's address.
 * The transaction must be signed by the raffle's organizer.

```plantuml
@startmindmap
!include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Mindmaps/0.Mindmap-AU.2-CreateRaffle-Transaction.puml
@endmindmap
```




##### Use Case [AU.3]: Buy tickets to raffle
---
**Actors**: Authenticated User, RaffleValidator    
**Summary Description**:	This transaction allows anyone to buy a ticket to an active raffle.  

**Preconditions:**
1. The raffle prize value must be locked to the raffle's validator address with valid datum.
2. User must have available funds for the tickets he wants to buy.
3. The deadline for buying tickets must not have passed. 

**Postconditions:**
1. Any value locked at the validator address, remains locked at the validator address (Transaction must have a single output).
2. A value containing the price for the tickets and the prize is locked in a single utxo at the raffle's validator address address.
3. The utxo has the datum updated with the tickets bought by the user and the secret hash of each ticket.  


**Transaction:**
```plantuml
@startmindmap
!include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Mindmaps/1.Mindmap-AU.3-BuyTickets-Transaction.puml
@endmindmap
```


##### Use Case [P.2]: Reveal Secrets for Raffle Tickets
---
**Actors**: Participant, RaffleValidator    
**Summary Description**:	This transaction allows any participant of a raffle to reveal the secret for it's ticket.  

**Preconditions:**
1. Participant has bought a one or more tickets for a raffle.
2. The deadline for buying tickets for the raffle has passed.
3. The participant knows the secrets for each ticket.
4. The transaction is signed by the owner of the ticket/s for which the secret/s is/are revealed.

**Postconditions:**
1. Any value locked at the script address, remains locked at the script address (Transaction must have a single output).
2. A value containing the price for the tickets and the prize is locked in a single utxo at the raffle's validator address address.
3. The utxo has the datum updated the secrets in clear for the tickets bought by the user, (each secret matches the hash of each ticket).   


**Transaction:**
```plantuml
@startmindmap
!include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Mindmaps/2.Mindmap-P.2-RevealSecrets-Transaction.puml
@endmindmap
```


##### Use Case [OP.1]: Close succesfully finalized raffle
---
**Actors**: Participant/Organizer, RaffleValidator    
**Summary Description**:	This transaction allows anyone to close a raffle for which the winner was elected.  

**Preconditions:**
1. The deadline for buying tickets has passed.
2. The deadline for reavealing ticket secrets has passed.
3. All participants revealed secrets so that the winner can be calculated.

**Postconditions:**
1. The prize is locked at the winning ticket owner's address.  
2. The amount representing the price for the sold tickets (-fee) is locked at the raffle's owner address.  
3. The fee + any ADA excess amount and assets of other asset classes which where locked at the ScriptAddress, must now be locked at the Donation address.  

**Transaction:**
```plantuml
@startmindmap
!include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Mindmaps/3a.Mindmap-OP.1-CloseWinnerSelectedByCRS-Transaction.puml
@endmindmap
```


##### Use Case [O.2]: Cancel raffle
---
**Actors**: Organizer, RaffleValidator    
**Summary Description**:	This transaction allows the organizer to cancel a raffle if buying tickets deadline has not passed and no tickets where bought yet.  

**Preconditions:**
1. The deadline for buying tickets has not passed.
2. No tickets where bought for the raffle.
3. The transaction is signed by the raffle owner.  
   
**Postconditions:**
1. The raffle prize value is locked at the raffle's owner address.  
2. Any ADA excess amount and assets of other asset classes which where locked at the ScriptAddress, must now be locked at the Donation address.  

**Transaction:**
```plantuml
@startmindmap
!include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Mindmaps/3b.Mindmap-O.2-CancelRaffle-Transaction.puml
@endmindmap
```


##### Use Case [O.3]: Close expired raffle
---
**Actors**: Organizer, RaffleValidator    
**Summary Description**:	This transaction allows the organizer to close a raffle if buying tickets deadline has passed and no tickets where bought yet.  

**Preconditions:**
1. The deadline for buying tickets has passed.
2. No tickets where bought for the raffle.
3. The transaction is signed by the raffle owner.

**Postconditions:**
1. The prize is locked at the raffle's owner address.  
2. Any ADA excess amount and assets of other asset classes which where locked at the ScriptAddress, must now be locked at the Donation address. 
    
**Transaction:**
```plantuml
@startmindmap
!include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Mindmaps/3b.Mindmap-O.3-CloseExpiredRaffle-Transaction.puml
@endmindmap
```

##### Use Case [O.4]: Close underfunded raffle
---
**Actors**: Organizer, RaffleValidator    
**Summary Description**:	This transaction allows the organizer to close an underfunded raffle.  

**Preconditions:**
1. The deadline for buying tickets has passed.
2. The number of tickets sold is higher than 0 and lower than the minimum no. of tickets set for the raffle.

**Postconditions:**
1. The prize is locked at the raffle's owner address.  
2. For each ticket sold and output with the price of the ticket is locked at the ticket owner's address. 
3. Any ADA excess amount and assets of other asset classes which where locked at the ScriptAddress, must now be locked at the Donation address.  

**Transaction:**
```plantuml
@startmindmap
!include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Mindmaps/3b.Mindmap-O.4-CloseUnderfundedRaffle-Transaction.puml
@endmindmap
```



##### Use Case [O.5]: Close unrevealed raffle  
---
**Actors**: Organizer, RaffleValidator    
**Summary Description**:	This transaction allows the organizer to close an unrevealed raffle.  

**Preconditions:**
1. The deadline for buying tickets has passed.
2. The deadline for revealing secrets has passed.
3. Not all participants revealed secrets so that the winner cannot be calculated.

**Postconditions:**
1. The prize is locked at the raffle's owner address.  
2. The total amount from unrevealed tickets is locked to the raffle's owner address.
3. For each ticket sold for which the secret was revealed, an output with the price of the ticket is locked at the ticket owner's address. 
4. Any ADA excess amount and assets of other asset classes which where locked at the ScriptAddress, must now be locked at the Donation address.  

**Transaction:**
```plantuml
@startmindmap
!include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Mindmaps/3b.Mindmap-O.5-CloseUnrevealedRaffle-Transaction.puml
@endmindmap
```
##### Use Case [AU.4a]: Close exposed underfunded raffle   
---
**Actors**: Any PKH, RaffleValidator    
**Summary Description**:	This transaction allows any user to close an underfunded raffle if the organizer did not closed it in time and get the NFT.  

**Preconditions:**
1. The deadline for buying tickets has passed.
2. The close deadline has passed.
3. The number of tickets sold is higher than 0 and lower than the minimum no. of tickets set for the raffle.

**Postconditions:**
1. The prize is locked at the address specified by the one who submitted the transaction.  
2. For each ticket sold and output with the price of the ticket is locked at the ticket owner's address. 
3. Any ADA excess amount and assets of other asset classes which where locked at the ScriptAddress, must now be locked at the Donation address.  

**Transaction:**
```plantuml
@startmindmap
!include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Mindmaps/4a.Mindmap-AU.4a-CloseExposedUnderfundedRaffle-Transaction.puml
@endmindmap
```

##### Use Case [AU.4b]: Close exposed unrevealed raffle
---
**Actors**: Any, RaffleValidator    
**Summary Description**:	This transaction allows the any user to close an unrevealed raffle if the organizer did not closed it in time and get the NFT and the collected amount for the unrevealed tickets. 

**Preconditions:**
1. The deadline for buying tickets has passed.
2. The deadline for revealing secrets has passed.
3. The close deadline has passed.
4. Not all participants revealed secrets so that the winner cannot be calculated.

**Postconditions:**
1. The prize is locked at the address specified by the one who submitted the transaction.  
2. The total amount from unrevealed tickets is locked to the address specified by the one who submitted the transaction.
3. For each ticket sold for which the secret was revealed, an output with the price of the ticket is locked at the ticket owner's address. 
4. Any ADA excess amount and assets of other asset classes which where locked at the ScriptAddress, must now be locked at the Donation address.  

**Transaction:**
```plantuml
@startmindmap
!include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Mindmaps/4b.Mindmap-AU.4b-CloseExposedUnrevealedRaffle-Transaction.puml
@endmindmap
```



### 2.3 🧑‍🤝‍🧑 User Classes and Characteristics

#### Casual Users
- **Characteristics**:
Likely to use the app infrequently.
Primarily interested in participating in raffles rather than organizing them.
May have basic technical expertise and limited understanding of blockchain and cryptocurrency.
- **Pertinent Functions**:
Viewing active raffles, buying tickets, and participating in raffles.
- **Importance**:
Essential for maintaining a broad user base, and maintaining active participation in raffles.

#### Raffle Organizers
- **Characteristics**:
Users with a higher level of involvement, responsible for creating and managing raffles.
Have a better understanding of the app's functionalities and may be more savvy with digital wallets and transactions.
Likely to have a good understanding of the app's full range of functionalities.
- **Pertinent Functions**:
Creating raffles, managing raffle lifecycle, closing expired raffles.
- **Importance**:
Extremely important as they are the primary source of content (raffles) in the app.



### 2.4 🏞️ Operating Environment
The environment in which the software will operate:  

**Hardware Platforms:**
- **Computers and Mobile Devices:**
The DApp should be optimized for both desktop and mobile devices, considering a range of specifications from high-end to more basic models.
Should support major manufacturers like Apple, Samsung, Lenovo, etc.  
- **Network Connectivity:** Requires a stable internet connection for blockchain interactions and data syncing.  

**Operating Systems** : Any operating systems which support major browsers.  Consideration for supporting native mobile OS applications: Android and iOS (latest and some previous versions).  
**Browsers:**
Since the DApp operates within a browser environment:
Support for major browsers like Chrome, Firefox, Safari, and Edge.
Consideration for browser updates and compatibility with new features.    
**Cardano Blockchain:** The DApp will interact with a specific blockchain, most commonly Cardano.  


### 2.5 🖌️⛔ Design and Implementation Constraints

#### Language Requirements

**Smart Contract Language:**
Use of Plutus for smart contracts. Consideration for alternative implementations in: Marlowe, Opshin, Aiken, Plutarch.   
**Frontend and Backend Development:**
Use functional programming languages for the DApp’s frontend and backend (e.g., Haskell, Purescript).

#### Security Considerations
**Smart Contract Security:** Audited and secure smart contracts to prevent vulnerabilities and exploits.  
**Testing:** Rigorous testing of the offchain code to prevent vulnerabilities.
#### Design Conventions or Programming Standards
**Code Quality and Maintainability:**
Adherence to industry-standard coding practices for readability, maintainability, and scalability.  
**Documentation:**
Comprehensive documentation for future maintenance and updates.  

### 2.6 📚 User Documentation
List the user documentation components (such as user manuals, on-line help, and tutorials) that will be delivered along with the software. Identify any known user documentation delivery formats or standards.
1. **Step-by-Step Guides:**  Visual and textual guides for key functions and features.  
2. **Video tutorials:** Instructional videos demonstrating how to use the DApp
3. **Legal and Compliance Information:**
   1. Terms of Service: Legal terms and conditions of using the DApp.
   2. Privacy Policy: Information on data handling, user privacy, and security measures.

### 2.7 🤔 Assumptions and Dependencies
The development and successful operation of the C-A-R-D-A-N-O-Raffles DApp are contingent upon several assumptions and dependencies. Understanding these factors is crucial as they could significantly impact the project's trajectory and its requirements.
#### Assumptions
**Blockchain Stability and Accessibility:** The DApp assumes consistent uptime and stability of the Cardano blockchain network. Access to the blockchain is presumed uninterrupted, with minimal downtime.  
**Smart Contract Reliability:** It is assumed that the smart contracts,  will perform as intended without major bugs or vulnerabilities.  
**Regulatory Compliance:** The assumption that current and future regulations regarding blockchain technology, cryptocurrencies, and online gambling or raffles will remain favorable or at least not become prohibitively restrictive.  
**Technological Proficiency of Users:** A basic level of understanding and comfort with blockchain technology and digital wallets among the user base is assumed.  
**Third-Party Wallet Integration:** Seamless integration and compatibility with popular third-party browser wallets are assumed for transaction signing and user authentication.  
**Internet Connectivity:** Users are assumed to have stable and continuous access to the internet, essential for interacting with the blockchain and the DApp.
 

#### Dependencies
**Cardano Blockchain Infrastructure:** for it's core functionality the smart contract execution.  
**Browser wallets**: for user interactions with the blockchain, such as signing transactions and managing digital assets.  
**Blockchain Service Providers:** for user interactions with the blockchain.  
**Web Hosting Services:** For versions 1 and 2 of the DApp, dependency on centralized web hosting services. For version 3, reliance shifts to decentralized hosting solutions like IPFS.  


## 3. 🔌 External Interface Requirements
### 3.1 💻 User Interfaces

#### General User Interface Characteristics:  
**Consistency:** The UI across the DApp maintains a consistent look and feel.   
**Responsive Design**: The UI adapts to different screen sizes and resolutions, providing an optimal experience on both desktop and mobile devices.   

#### Key Components of the User Interface:

--- 
- **Dashboard/Main Screen** 
  - **Layout:** A clean and intuitive layout displaying active raffles, with clear navigation menus for accessing other parts of the DApp.  
  - **Information Display:** Active raffles are presented with essential information like asset details, ticket price, and time remaining.  
  - **Interaction:** Easy access to join raffles or view more details with minimal clicks/taps.  
--- 
```plantuml
@startsalt
  !include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Wireframes/UI-Dashboard-NonAuth.puml
@endsalt
```
---
```plantuml
@startsalt
  !include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Wireframes/UI-Dashboard-Auth.puml
@endsalt
```
--- 

- **Raffle Detail Page**
  - **Information Display:**  Provides comprehensive information about a specific raffle state, including the asset being raffled, total number of participants, and the smart contract details.
  - **Interaction:**: A clear and secure way to purchase tickets for raffle.

--- 
```plantuml
@startsalt
  !include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Wireframes/UI-RaffleDetails-Commit.puml
@endsalt
```
---
```plantuml
@startsalt
  !include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Wireframes/UI-RaffleDetails-Reveal.puml
@endsalt
```
--- 

- **User Profile/Account Management**
  - **Information Display:** 
    - My Assets: Display current assets owned by the connected wallet.
    - My Tickets: Display current participations in raffles.
    - Transaction History: Display of past transactions and participations in raffles.
  - **Interaction:**  
    - Wallet Integration: Easy access for connecting or disconnecting digital wallets.
    - Create raffle: Possibility to create a raffle for a selected asset.
    - 
```plantuml
@startsalt
  !include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Wireframes/UI-RaffleProfile-MyAssets.puml
@endsalt
```
```plantuml
@startsalt
  !include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Wireframes/UI-RaffleProfile-MyTickets.puml
@endsalt
```

--- 

- **Raffle Creation Interface**
  - **Information Display:** Intuitive Forms: Simple forms to input details for creating a new raffle, such as asset details, ticket price, and duration.
  - **Interaction:**
    - Guidance and Help: Inline tips or help buttons to guide organizers through the raffle creation process.
    - Preview and Confirmation: Before submission, a preview of the raffle for review and confirmation.

```plantuml
@startsalt
  !include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Wireframes/UI-RaffleProfile-Create.puml
@endsalt
```
--- 


- **Help and FAQ Sections**: Easily accessible help sections and FAQs for user assistance.
--- 


### 3.2 🖥️ Software Interfaces

- API exposed by the Backend
- Constructing transactions by hand (with cardano cli)

## 4. System Features

---
---
### US-1: View active raffles
- **As a**  User, 
- **I want** to view active raffles  initiated by other users,
- **so that** I can choose to participate in one of them.
---
#### Acceptance Criteria.

- **Scenario:** A user visits the Dapp.
  - **Given** the user is in a superted web browser .
  - **When**  the user visits the DApp web address.
  - **Then**  
    - the following elements should be available:
      -  An option to select a wallet to connect with;
      -  A section where all active raffles are displayed, with an option to buy a ticket;
      -  A search bar for searching assets;
      -  An option to create a raffle;
---
---
### US-2: Connect wallet
- **As a**  User, 
- **I want** connect with my wallet,
- **so that** I can be able to buy tickets or create raffles.
---
- **Scenario:** Connecting a wallet.
   - **Given** 
     - the user is in the DApp.
     - the user has not connected to a wallet yet.
   - **When**  the user presses on [Connect wallet] button.
   - **Then**  a list of a supported wallets should be displayed highliting the ones installed on the user's browser.

   - **Given** the user has a supported wallet installed.
   - **When**  the user selects the supported wallet and connects to the DApp.
   - **Then**  [Connect wallet] button will be replaced with the address of the user's wallet an an option to switch/disconnect. 
  
---
---
### US-3:  Create a raffle
- **As a** User,
- **I want** to create a raffle for one or more digital assets I own,
- **so that** other users can buy chances (tickets) to win the asset/s and I'll get the funds in exchange.
---
#### Acceptance Criteria.

- **Scenario**: Create raffle 
  - **Given** the user has connected the wallet;
  - **When**  the user clicks on "Create new raffle";
  - **Then** a new page should be displayed allowing the possibility  to select the digital asset/s which is/are subject of the raffle;  
  
  - **Given** Digital asset/s is/are selected for a given raffle.
  - **When** Confirms selection
  - **Then** a form with the following input fields
          - the ticket price value;
          - total no. of tickets;
          - min. no. of tickets to be sold for the raffle to be valid;
          - the deadline for participating in the raffle;
          - the deadline for revealing ticket secret hashes;

  - **Given** Raffle details form is displayed.
  - **When** All input fields are filled in with valid values.
  - **Then** The "Create Raffle" button should become available.

  - **Given** "Create Raffle" button is available
  - **When** "Create Raffle" is pressed, transaction signed and submitted.
  - **Then** user must be returned to his "MyRaffles" page, where previously created raffle should be displayed besides his existing raffles.

---
---     
### US-3: Buy ticket to raffle
**As a** User,  
**I want** to participate to a raffle,  
**so that** I have a chance to win the prize.  

#### Acceptance Criteria.

- **Scenario**: Buy ticket
  - **Given** the user has connected the wallet;
  - **When** user clicks on the "Buy Ticket" button next to the raffle he chose.
  - **Then** a new page should be displayed containing the following elements:
    - Raffle info (tbd);
    - an input numeric field  ("No of tickets") for user to select the no. of tickets he wants to buy, by default filled with value 1;
    - the total cost of the tickets;
    - an input field for the secret hash.

  - **Given** the user selected the no. of tickets and provided a secret hash;
  - **When** [Buy ticket] button is pressed, transaction signed and submitted.
  - **Then** a new page should be displayed containing the following elements:

- **Scenario**: Not logged in user picks a raffle and wants to buy tickets
    - **Given** User is not logged in
    - **When** user clicks on the "Buy Ticket" button next to the raffle he chose.
    - **Then** user must be returned to his "MyTickets" page, where the new tickets are displayed besides his existing ones.
---
---
### US-4:  View my raffles

**As a** Raffle Organizer,    
**I want** to view all the raffles that I have created,   
**so that** I can manage and keep track of their progress.   

#### Acceptance Criteria.

- **Scenario**: Accessing the My Raffles page
  - **Given** the user is authenticated and has previously created one or more raffles,
  - **When** the user navigates to the "My Raffles" section,
  - **Then** the user should see a list of all raffles they have created, with details such as:
    - Raffle title and description,
    - Number of tickets sold,
    - Total number of tickets,
    - Status of the raffle (e.g., active, closed, pending),
    - Deadline for the raffle.

- **Scenario:** Interacting with listed raffles
  - **Given** the user is viewing their raffles in the "My Raffles" section,
  - **When** the user selects a specific raffle,
  - **Then** they should be able to view detailed information about the raffle and perform actions like cancel or close the raffle, if applicable.


- **Scenario**: No raffles created
  - **Given** the user has not created any raffles,
  - **When** the user navigates to the "My Raffles" section,
  - **Then** a message should be displayed indicating that no raffles have been created yet, along with a prompt or button to create a new raffle.

---
---
### US-5:  View my tickets
**As a** Participant,  
**I want** to view all the tickets I have purchased for various raffles,  
**so that** I can keep track of my participation and potential winnings.  

#### Acceptance Criteria.

- **Scenario**: Accessing the My Tickets page
  - **Given** the user is authenticated and has previously purchased one or more raffle tickets,
  - **When** the user navigates to the "My Tickets" section,
  - **Then** the user should see a list of all tickets they have purchased, with details such as:
    - Raffle associated with each ticket,
    - Number of tickets purchased in each raffle,
    - Status of each ticket (e.g., commit, reveal, expired),
    - Deadline

- **Scenario**: Viewing details of a specific ticket
  - **Given** the user is viewing their tickets in the "My Tickets" section,
  - **When** the user selects a specific ticket,
  - **Then** they should be able to view detailed information about the ticket, including the raffle's outcome if it has already been drawn.

- **Scenario**: No tickets purchased
  - **Given** the user has not purchased any tickets,
  - **When** the user navigates to the "My Tickets" section,
  - **Then** a message should be displayed indicating that no tickets have been purchased yet, along with suggestions or links to active raffles for participation.

---
---

## 📄 Appendix A: Analysis Models
#### State Diagram
```plantuml
@startuml
  !include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/State/StateDiagram-RaffleValidator.puml
@enduml
```
#### Class Diagram
```plantuml
@startuml
  !include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Class/ClassDiagram-Conceptual-Raffle.puml
@enduml
```

#### Sequence Digram
```plantuml
@startuml
  !include https://gitlab.com/mariusgeorgescu1/c-a-r-d-a-n-o-raffles/-/raw/main/Documentation/Diagrams/Sequence/RaffleDApp-Sequence.puml
@enduml
```


## 📄 Appendix B: Glossary

#### Definitions, Acronyms and Abbreviations
##### Definitions
- Digital Asset: A digital representation of value or contractual rights, which can be traded and transferred without intermediaries.
- Raffle: A game or contest in which tokens or tickets, that have been sold, are drawn at random to determine the winner of a prize.
- Secret Hash: A unique cryptographic code that represents the ticket in the raffle.
- Wallet: A software-based system that securely stores users' digital assets and allows for transactions.
##### Acronyms and Abbreviations

| Acronym               | Definition                                                                                                                                              |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- |
| DApp                  | Decentralized Application                                                                                                                               |
| C-A-R-D-A-N-O-Raffles | Commit-And-Reveal-Decentralized-Application-NFT-Outlet Raffles                                                                                          |
| CRS                   | Commit and Reveal Scheme                                                                                                                                |
| IPFS                  | InterPlanetary File System : is a protocol, hypermedia and file sharing peer-to-peer network for storing and sharing data in a distributed file system. |
| NFT                   | Non-Fungible Token                                                                                                                                      |



 
